class App {

    constructor(products, container_id){

        this.cart = new Cart(products);

        this.products = products;
        this.container = document.getElementById(container_id);
        this.showProducts();
    }

    showProducts(){
 
        for(var j in this.products){

            var data = this.products[j];

             var product = new ProductContent(data, this.cart.add);

         this.container.appendChild(product.render());
         }
    }
}

class Cart {
    constructor(products){

        this.productsCart = [];
        this.products = products;

        this.add = this.add.bind(this);
        this.remove = this.remove.bind(this);
        this.show = this.show.bind(this);

        this.container = document.getElementById('cart');
        this.buttonBlock = document.getElementById('show_cart');

        this.buttonBlock.onclick = this.show;
        //this.buttonBlock.onmouseleave = this.show;

        this.fullSum = 0 ;
        this.fullQuantity = 0 ;
    }

    show(){
        this.container.classList.toggle("d-none");
    }

    add(e){
        var id = + e.target.dataset.productId;
        var Product = this.isProductCart(id);

        if(Product){
            Product.quantity ++;
            Product.sum += Product.price; 
        }else{
            Product = this.getProduct(id);
            Product.sum = Product.price;
            Product.quantity = 1 ;
            this.productsCart.push(this.getProduct(id))
        }

        this.fullQuantity ++;
        this.fullSum += Product.price;


        this.render();
    }
    remove(e)
    {
        var id = + e.target.dataset.productId;
        var Product = this.isProductCart(id);

        if(Product && Product.quantity > 0){ //должно убирать полностью , поставил ноль  что бы обнулялось ,  но не убирается 
            Product.quantity --;
            Product.sum -= Product.price; 
        }else{
            this.removeProduct(id);
        }
        this.fullQuantity --;
        this.fullSum -= Product.price;
        
        this.render();

    }
    isProductCart(id){
        for(var j in this.productsCart){
            if(+ this.productsCart[j]['id'] === id){

                return this.productsCart[j];
            }
        }
        return false;
    }
    getProduct(id)
    {
        for(var j in this.products){
            if(+ this.products[j]['id'] === id){

                return this.products[j];
            }
        }
    }
    removeProduct(id)
    {
       
        this.productsCart = this.productsCart.filter((product)=>{
            if(+products[j]['id'] === id){

                return false;
            }
            return true ;
        
        });
    }
    render()
    {
        this.container.innerHTML = "";
        for(var j in this.productsCart)
        {
            var product = new ProductCart(this.productsCart[j],this.remove);

            this.container.appendChild(product.render());
        }
        this.buttonBlock.innerHTML = 'Full Sum :' + this.fullSum +'<br/>' +'Full Quantity :' + this.fullQuantity;

    }
}

class Product{
    constructor(dataProduct, callback){

        this.data = dataProduct;

        this.container = document.createElement('div');

        this.name = document.createElement('h4'); 
        this.image = document.createElement('img');
        this.price = document.createElement('p');
        this.button = document.createElement('button');

        this.name.className         = 'p-name';
        this.image.className        = 'p-img img-fluid';
        this.price.className        = 'p-price';
        this.button.className       = 'btn btn-primary';
        this.button.type            = 'button';

        this.name.innerHTML         = this.data.name;
        this.image.src              =this.data.image;
        this.price.innerHTML        = this.data.price;
        this.button.setAttribute('data-product-id', this.data.id);

        if(callback){
            this.button.onclick = callback;
        }
    }
}


class ProductContent extends Product{
    
    constructor(dataProduct,callback){
        super(dataProduct, callback);
    }


    render() {
        this.container.className          = 'content-product col-sm-4 text-center mt-4';//mt- margin-top
        this.button.innerHTML       = 'To cart';
        this.container.appendChild(this.name);
        this.container.appendChild(this.image);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }
}

class ProductCart extends Product{
    constructor(dataProduct,callback){
        super(dataProduct, callback);
    }
    render() {
        this.container.className     = 'cart-product align-items-center row mt-2';//mt- margin-top
        this.button.innerHTML        = 'Delete';
        

        this.name.className += 'col-sm-3';// 3 в ряд должно быть 
        this.price.className += 'col-sm-3';
        

        this.quantity = document.createElement('p');
        this.quantity.className = 'col-sm-2';
        this.quantity.innerHTML = this.data.quantity;
        this.price.innerHTML = this.data.sum;

        this.container.appendChild(this.name);
        this.container.appendChild(this.quantity);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

       

        return this.container;
    }
}

var app = new App (products, 'products-block');




/*1.0
var product = new Product(pp);

var cont = document.getElementById('products-block');

cont.appendChild(product.render());*/