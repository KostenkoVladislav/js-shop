/*var categories = [
    {
        name : 'Phones' , link:'#', children: 
        [
            {
            name : 'Nokia' , link:'#',products:
            [
                {
                id :'1',
                name: 'Nokia 5',
                manufacturer:'China',
                price: 5000,
                image :'product.jpg',
                attr:{
                    camera: '5mp',
                    diagonal:'5.5',
                    os:'android',
                }
            },
            {
                id :'2',
                name: 'Nokia 6',
                manufacturer:'China',
                price: 7000,
                image :'product.jpg',
                attr:{
                    camera: '7mp',
                    diagonal:'6',
                    os:'android',
                }
            },
            {
                id :'3',
                name: 'Nokia 7',
                manufacturer:'China',
                price: 9000,
                image :'product.jpg',
                attr:{
                    camera: '10mp',
                    diagonal:'7',
                    os:'android',
                }
            }
        ]
        }
    ]
    },
    {
        name : 'Laptops' , link:'#', children: 
        [
            {
            name : 'Dell' , link:'#',products:
            [
                {
                id :'4',
                name: 'Dell Ex505',
                manufacturer:'China',
                price: 15000,
                image :'product.jpg',
                attr:{
                    camera: '3mp',
                    diagonal:'15.5',
                    os:'Windows',
                }
            },
            {
                id :'5',
                name: 'Dell Ex510',
                manufacturer:'China',
                price: 17000,
                image :'product.jpg',
                attr:{
                    camera: '5mp',
                    diagonal:'17.5',
                    os:'Linux',
                }
            },
            {
                id :'6',
                name: 'Dell Ex600',
                manufacturer:'China',
                price: 19000,
                image :'product.jpg',
                attr:{
                    camera: '5mp',
                    diagonal:'17',
                    os:'Windows',
                }
            },
        ]
        }
        
        
    ]
    },
    {
        name : 'Laptops' , link:'#', children: 
        [
            {
            name : 'Samsung' , link:'#',products:
            [
                {
                id :'7',
                name: 'Samsung Io505',
                manufacturer:'China',
                price: 15000,
                image :'product.jpg',
                attr:{
                    camera: '3mp',
                    diagonal:'15.5',
                    os:'Windows',
                }
            },
            {
                id :'8',
                name: ' Samsung Io510',
                manufacturer:'China',
                price: 17000,
                image :'product.jpg',
                attr:{
                    camera: '5mp',
                    diagonal:'17.5',
                    os:'Linux',
                }
            },
            {
                id :'9',
                name: ' Samsung Io600',
                manufacturer:'China',
                price: 19000,
                image :'product.jpg',
                attr:{
                    camera: '5mp',
                    diagonal:'17',
                    os:'Windows',
                }
            },
        ]
        }
        
        
    ]
    }
];*/



var products = [
    {
        id :'1',
        name: 'Nokia 5',
        manufacturer:'China',
        price: 5000,
        image :'product.jpeg',
        attr:{
            camera: '5mp',
            diagonal:'5.5',
            os:'android',
        }
    },
    {
        id :'2',
        name: 'Nokia 6',
        manufacturer:'China',
        price: 7000,
        image :'product.jpeg',
        attr:{
            camera: '7mp',
            diagonal:'6',
            os:'android',
        }
    },
    {
        id :'3',
        name: 'Nokia 7',
        manufacturer:'China',
        price: 9000,
        image :'product.jpeg',
        attr:{
            camera: '10mp',
            diagonal:'7',
            os:'android',
        }
    },
    {
        id :'4',
        name: 'Dell Ex505',
        manufacturer:'China',
        price: 15000,
        image :'product.jpeg',
        attr:{
            camera: '3mp',
            diagonal:'15.5',
            os:'Windows',
        }
    },
    {
        id :'5',
        name: 'Dell Ex510',
        manufacturer:'China',
        price: 17000,
        image :'product.jpeg',
        attr:{
            camera: '5mp',
            diagonal:'17.5',
            os:'Linux',
        }
    },
    {
        id :'6',
        name: 'Dell Ex600',
        manufacturer:'China',
        price: 19000,
        image :'product.jpeg',
        attr:{
            camera: '5mp',
            diagonal:'17',
            os:'Windows',
        }
    },
    {
        id :'7',
        name: 'Samsung Io505',
        manufacturer:'China',
        price: 15000,
        image :'product.jpeg',
        attr:{
            camera: '3mp',
            diagonal:'15.5',
            os:'Windows',
        }
    },
    {
        id :'8',
        name: ' Samsung Io510',
        manufacturer:'China',
        price: 17000,
        image :'product.jpeg',
        attr:{
            camera: '5mp',
            diagonal:'17.5',
            os:'Linux',
        }
    },
    {
        id :'9',
        name: ' Samsung Io600',
        manufacturer:'China',
        price: 19000,
        image :'product.jpeg',
        attr:{
            camera: '5mp',
            diagonal:'17',
            os:'Windows',
        }
    }
];